const APP_HEIGHT = 600;
const APP_WIDTH = 800;
const BALL_RADIUS = 40;
const BALL_START = Object.freeze({
  x: APP_WIDTH - 150,
  y: APP_HEIGHT - 150,
});
const BALL_TOTAL_TRAVEL = 50; // pixels
const JACKS_DEFAULT_AMOUNT = 4;
const JACKS_GRID_DIMENSIONS = Object.freeze({
  x: 4,
  y: 4,
});
const JACKS_GRID_MARGIN = {
  x: APP_WIDTH / 4,
  y: APP_HEIGHT / 3,
};
const JACKS_GRID_OFFSET = 65;

// Generate jacks grid spacing
const _temp_jacks_grid_spacing = {
  x: [],
  y: [],
};
for (let i = 0; i < JACKS_GRID_DIMENSIONS.x; i++) {
  _temp_jacks_grid_spacing.x.push(JACKS_GRID_MARGIN.x + APP_WIDTH * (i / 7));
}
for (let i = 0; i < JACKS_GRID_DIMENSIONS.y; i++) {
  _temp_jacks_grid_spacing.y.push(JACKS_GRID_MARGIN.y + APP_HEIGHT * (i / 7));
}
const JACKS_GRID_SPACING = Object.freeze({
  x: Object.freeze(_temp_jacks_grid_spacing.x),
  y: Object.freeze(_temp_jacks_grid_spacing.y),
});
const JACKS_SIZE = Object.freeze({
  x: 112,
  y: 112,
});
const JACKS_ROW_SIZE_OFFSET = Object.freeze([
  .8,
  .8,
  .9,
  1
]);
const JACKS_SPEED_ON_START = 20;

const MAX_JACKS = JACKS_GRID_DIMENSIONS.x * JACKS_GRID_DIMENSIONS.y;
const MESSAGES = Object.freeze({
  LOSE: 'You lose!',
  WIN: 'You win!',
});
const OFFSCREEN_DISTANCE_PIXELS = {
  // I came to this value somewhat arbitrarily
  x: 300,
  y: 800,
};
const OFFSET_PIXEL_UNIT = 100;
// for loading into the background on app start
const PICTURES = [
  './assets/picture1.png',
  './assets/picture2.png',
];
const SECONDS_IN_AIR_BY_DIFFICULTY = Object.freeze({
  easy: 4,
  medium: 3,
  hard: 2,
});
const SHADOW_START = Object.freeze({
  x: BALL_START.x + (BALL_RADIUS / 2),
  y: BALL_START.y + (BALL_RADIUS * 2),
});

// Game engine meta variables
const SECONDS_IN_MILLISECONDS = 1000;
const FRAMES_PER_SECOND = 60;
const FRAME = Math.ceil(SECONDS_IN_MILLISECONDS / FRAMES_PER_SECOND);

export default {
  APP_HEIGHT,
  APP_WIDTH,
  BALL_RADIUS,
  BALL_START,
  BALL_TOTAL_TRAVEL,
  FRAME,
  FRAMES_PER_SECOND,
  JACKS_DEFAULT_AMOUNT,
  JACKS_GRID_DIMENSIONS,
  JACKS_GRID_MARGIN,
  JACKS_GRID_OFFSET,
  JACKS_GRID_SPACING,
  JACKS_ROW_SIZE_OFFSET,
  JACKS_SIZE,
  JACKS_SPEED_ON_START,
  MAX_JACKS,
  MESSAGES,
  OFFSCREEN_DISTANCE_PIXELS,
  OFFSET_PIXEL_UNIT,
  PICTURES,
  SECONDS_IN_AIR_BY_DIFFICULTY,
  SECONDS_IN_MILLISECONDS,
  SHADOW_START,
};
