import constants from "./constants.mjs";

/**
 * @param {*} pixiInstance
 * @returns {Object} A Pixi Container instance containing the background
 * sprite and picture sprite.
 */

export function makeBackground(pixiInstance) {
  const container = new pixiInstance.Container();
  const backgroundTexture = new pixiInstance.Texture.from('./assets/background.png');
  const background = new pixiInstance.Sprite(backgroundTexture);
  background.width = constants.APP_WIDTH;
  background.height = constants.APP_HEIGHT;
  container.addChild(background);

  const pictureBackground = new pixiInstance.Graphics();
  pictureBackground.beginFill(0x333333);
  pictureBackground.drawRect(0, 0, 131, 97);
  pictureBackground.endFill();
  pictureBackground.x = 579;
  pictureBackground.y = 28;
  container.addChild(pictureBackground);

  // Get random picture
  const pictureURL = constants.PICTURES[Math.floor(Math.random() * constants.PICTURES.length)];
  const pictureTexture = new pixiInstance.Texture.from(pictureURL);
  const picture = new pixiInstance.Sprite(pictureTexture);
  picture.x = 579;
  picture.y = 28;
  picture.width = 131;
  picture.height = 97;
  container.addChild(picture);

  return container;
}

export default {
  makeBackground,
};

