import constants from './constants.mjs';

/**
 * @param {*} pixiInstance
 * @return {Object} A Pixi Graphics instance of the ball's shadow
 */

export function makeShadow(pixiInstance) {
  const shadow = new pixiInstance.Graphics();
  shadow.beginFill(0x333333);
  shadow.drawEllipse(0, 0, (constants.BALL_RADIUS * 1.25), (constants.BALL_RADIUS / 4));
  shadow.endFill();
  shadow.x = constants.SHADOW_START.x;
  shadow.y = constants.SHADOW_START.y;
  return shadow;
}

/**
 * @param {*} pixiInstance
 * @return {Object} A Pixi Sprite instance of the ball
 */

export function makeBall(pixiInstance) {
  const ballImage = new pixiInstance.Texture.from('./assets/ball.png');
  const ball = new pixiInstance.Sprite(ballImage);
  ball.x = constants.BALL_START.x;
  ball.y = constants.BALL_START.y;
  ball.width = constants.BALL_RADIUS * 2;
  ball.height = constants.BALL_RADIUS * 2;
  ball.interactive = true;
  ball.zIndex = 100;
  return ball;
}

export default {
  makeBall,
  makeShadow,
};

