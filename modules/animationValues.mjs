import constants from './constants.mjs';

/**
 * @typedef {Object} AnimationValues
 * @property {Number} maxHeight
 * @property {Number} xStep
 * @property {Number} deltaStep
 */

/**
 * @param {Number} secondsInAir
 * @returns {AnimationValues}
 */

export function getBallAnimationValues(secondsInAir) {
  return {
    maxHeight: 14.25 / secondsInAir,
    xStep: constants.BALL_TOTAL_TRAVEL / (secondsInAir * constants.FRAMES_PER_SECOND),
    deltaStep: Math.PI / (constants.FRAMES_PER_SECOND * secondsInAir),
  };
}

export default {
  getBallAnimationValues,
};

