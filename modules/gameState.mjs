/**
 * @typedef {Object} GameState
 * @property {GameStateData} _data
 * @property {Object} _listeners
 * @property {Function} addSetListener
 * @property {Function} allJacksGrabbed
 * @property {Function} allJacksPlaced
 * @property {Function} get
 * @property {Function} isInSession
 * @property {Function} removeSetListener
 * @property {Function} set
 */

/**
 * @typedef {Object} GameStateData
 * @property {AnimationValues} ballAnimationValues
 * @property {Boolean} ballIsGrabbed
 * @property {Number} currentLevel
 * @property {String} difficulty
 * @property {Boolean} inSession
 * @property {Jack[]} jacks
 * @property {Number} score
 * @property {Number} startingLevel
 */

/**
 * @returns {GameState}
 */
export default function makeGameState() {
  return Object.freeze({
    _data: {
      ballAnimationValues: {},
      ballIsGrabbed: false,
      currentLevel: 1, // current level during gameplay
      difficulty: 'easy',
      inSession: false,
      jacks: [],
      score: 0,
      startingLevel: 1,
    },
    _listeners: {},

    /**
     * @param {String} property
     * @param {Function} callback
     * @return {void}
     */
    addSetListener(property, callback) {
      if (typeof this._data[property] === 'undefined') {
        console.error(`Property ${property} does not exist.`);
        return;
      }

      if (typeof this._listeners[property] === 'undefined') {
        this._listeners[property] = [callback];
      } else {
        this._listeners[property].push(callback);
      }
    },

    /**
     * @returns {Boolean}
     */
    allJacksGrabbed() {
      return this._data.jacks.every(jack => jack.grabbed);
    },

    /**
     * @returns {Boolean}
     */
    allJacksPlaced() {
      return this._data.jacks.every(jack => jack.isPlaced);
    },

    /**
     * @param {String} property
     * @returns {*}
     */
    get(property) {
      if (typeof this._data[property] === 'undefined') {
        return console.error(`Property ${property} does not exist.`);
      } else {
        return this._data[property];
      }
    },

    /**
     * @return {Boolean}
     */
    isInSession() {
      return this._data.inSession;
    },

    /**
     * @param {String} property
     * @param {Function} callbackToRemove
     * @return {void}
     */
    removeSetListener(property, callbackToRemove) {
      if (typeof this._data[property] === 'undefined') {
        console.error(`Property ${property} does not exist.`);
        return;
      }

      this._listeners[property] = this._listeners[property].filter(listener => listener !== callbackToRemove);
    },
    set(property, value) {
      if (typeof this._data[property] === 'undefined') {
        console.error(`Property ${property} does not exist.`);
      } else {
        this._data[property] = value;
      }

      this._listeners[property]?.forEach(callback => callback(value));
    },
  });
}
