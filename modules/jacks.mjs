import constants from "./constants.mjs";

/**
 * A jack that is in game. Is a Pixi Sprite instance, as well, but I'm not going
 * to include all of the Pixi properties here.
 *
 * @typedef {Object} Jack
 * @property {Object} delta
 * @property {Number} delta.x
 * @property {Number} delta.y
 * @property {Object} end
 * @property {Number} end.x
 * @property {Number} end.y
 * @property {Object} grid
 * @property {Number} grid.x
 * @property {Number} grid.y
 * @property {Object} start
 * @property {Number} start.x
 * @property {Number} start.y
 * @property {Number} id
 * @property {Boolean} grabbed
 * @property {Boolean} inPlay
 */

/**
 * @param {Number} currentLevel
 * @param {Jack[]} jacks
 * @returns {Boolean}
 */

export function correctNumberOfJacksGrabbed(currentLevel, jacks) {
  const jacksInPlay = jacks.filter(jack => jack.inPlay);
  if (jacksInPlay.length < currentLevel) {
    return jacksInPlay.every(jack => jack.grabbed);
  } else {
    const jacksGrabbed = jacksInPlay.filter(jack => jack.grabbed).length;
    return jacksGrabbed === currentLevel;
  }
}

/**
 * @typedef {Object} Position
 * @property {Number} x
 * @property {Number} y
 */

/**
 * @param {Number} numberOfJacks
 * @returns {Position[]}
 */

export function getRandomPositions(numberOfJacks) {
  if (numberOfJacks > constants.MAX_JACKS) {
    return [];
  }

  const {
    x: MAX_X,
    y: MAX_Y,
  } = constants.JACKS_GRID_DIMENSIONS;

  const positions = [];
  const gridSet = new Set();

  for (let i = 0; i < numberOfJacks; i++) {
    let hash, x, y;
    while (true) {
      x = Math.floor(Math.random() * MAX_X);
      y = Math.floor(Math.random() * MAX_Y);
      // Hash: y = Math.floor(hash / MAX_Y); x = hash % 4;
      hash = (MAX_Y * y) + x;
      if (!gridSet.has(hash)) {
        break;
      }
    }

    gridSet.add(hash);
    positions.push({ x, y });
  }

  return positions;
}

/**
 * @param {Number} numberOfJacks The number of jacks to be generated on the 4x4 grid
 * @param {*} pixiInstance The PIXI library
 * @returns {Jack[]} An array of PIXI jack sprites to be put into the gamestate
 */

export function makeJacks(numberOfJacks, pixiInstance) {
  const positions = getRandomPositions(numberOfJacks);

  const allJacks = [];

  for (let i = 0; i < numberOfJacks; i++) {
    const jackImage = new pixiInstance.Texture.from('./assets/jack.png');
    const jack = new pixiInstance.Sprite(jackImage);
    const { x: gridX, y: gridY } = positions[i];
    jack.grid = {
      x: gridX,
      y: gridY,
    };

    const xOffsetByRow = constants.JACKS_GRID_OFFSET * gridY;
    jack.end = {
      x: constants.JACKS_GRID_SPACING.x[gridX] - xOffsetByRow,
      y: constants.JACKS_GRID_SPACING.y[gridY],
    };
    const yOffset = (gridY - constants.JACKS_GRID_DIMENSIONS.y) * constants.OFFSET_PIXEL_UNIT;
    jack.start = {
      x: jack.end.x - constants.OFFSCREEN_DISTANCE_PIXELS.x,
      y: jack.end.y + constants.OFFSCREEN_DISTANCE_PIXELS.y + yOffset,
    };
    jack.x = jack.start.x;
    jack.y = jack.start.y;
    jack.delta = {
      x: jack.end.x - jack.start.x,
      y: jack.end.y - jack.start.y,
    };
    jack.interactive = true;
    jack.inPlay = true;
    jack.grabbed = false;
    jack.isPlaced = false;
    jack.visible = true;
    jack.id = i;
    jack.width = 0;
    jack.height = 0;
    allJacks.push(jack);
  }

  return allJacks;
}

/**
 * @param {Jack[]} jacks
 * @return {void}
 */

export function moveGrabbedJacksOutOfPlay(jacks) {
  if (!Array.isArray(jacks)) {
    return;
  }

  jacks.forEach(jack => {
    if (jack.grabbed) {
      jack.inPlay = false;
    }
  });
}

export default {
  correctNumberOfJacksGrabbed,
  getRandomPositions,
  makeJacks,
  moveGrabbedJacksOutOfPlay,
};
