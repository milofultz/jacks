import constants from './constants.mjs';

PIXI.settings.SORTABLE_CHILDREN = true;

const app = document.getElementById('app');
const canvas = document.createElement('canvas');

const pixi = new PIXI.Application({
  width: constants.APP_WIDTH,
  height: constants.APP_HEIGHT,
  view: canvas,
});

app.appendChild(canvas);

export default pixi;
