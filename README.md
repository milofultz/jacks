# Jacks

I used to play this game that my dad got from his old job. It was just the game of [jacks](https://archive.org/details/JACKS_201612) on Windows 3.1, clicking around a ton with the mouse. It was chaotic and fun for about 5 minutes at a time, so why not do it again.

## Plan

* ~~Determine tech stack~~
* ~~Render a ball graphic~~
* ~~Make that ball graphic alert on click~~
* ~~Make that ball bounce on click~~
* ~~Render a jack-like object~~
* ~~Make that jack-like object do something on click after clicking on ball~~
* ~~Make background image like the original game (room, picture frame)~~
* ~~Make ball graphic more like the game (gradients, light reflection)~~
* ~~Use actual jacks rules and not what I remembered when thinking about it~~
* ~~Add some kind of pub/sub for gameState changes? So far would be useful for changes in levels, win/lose, maybe others?~~
* Put ball and ball shadow into a container
* Add level change (dropdown, then in taskbar)
* Add a jack placing function, extract existing in app.js
* Make shadow more accurate to a different light angle
* Add difficulty levels (dropdown, then taskbar)
* Add black and white mode
* Make canvas maximum size and otherwise resizable
* Make game possible on mobile?
* Make an SVG of a jack and a ball that can be modified into PIXI
* Create a dialog box for win/lose (like Windows 3.1)
* Implement a win/lose mechanic on click
