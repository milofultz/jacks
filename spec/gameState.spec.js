import makeGameState from "../modules/gameState.mjs";
import mockPixi from './mocks/mockPixi.js';
import { makeJacks } from "../modules/jacks.mjs";

// To remove annoying console errors in testing
global.console.error = () => {};

describe('makeGameState', () => {
  let gameState;

  beforeEach(() => {
    gameState = makeGameState();
  });

  it('should make a new object with getters and setters', () => {
    expect(gameState).toEqual(jasmine.any(Object));
    expect(gameState).toEqual(jasmine.objectContaining({
      _data: jasmine.any(Object),
      get: jasmine.any(Function),
      set: jasmine.any(Function),
    }));
  });

  describe('get and set', () => {
    const nonExistentKey = 'nonExistentKey';
    const existentKey = 'score';

    it('should not allow setting of non-existent properties in the data and throw an error', () => {
      // Key shouldn't exist in the data to start with
      expect(gameState._data[nonExistentKey]).toBeUndefined();

      spyOn(console, 'error');
      const beforeData = gameState._data;
      gameState.set(nonExistentKey, 'nothing');
      const afterData = gameState._data;

      // Data should not have changed
      expect(beforeData).toEqual(afterData);
      // Should have thrown an error
      expect(console.error).toHaveBeenCalled();
    });

    it('should throw an error when retrieving non-existent keys', () => {
      // Key shouldn't exist in the data to start with
      expect(gameState._data[nonExistentKey]).toBeUndefined();

      spyOn(console, 'error');
      gameState.get('stupidProperty');

      // Should have thrown an error
      expect(console.error).toHaveBeenCalled();
    });

    it('should set existing properties of data and return undefined', () => {
      // Key should exist
      expect(gameState._data[existentKey]).not.toBeUndefined();

      const beforeData = gameState._data[existentKey];
      const result = gameState.set(existentKey, 100);
      const afterData = gameState._data[existentKey];

      expect(beforeData).not.toEqual(afterData);
      expect(result).toBeUndefined();
    });

    it('should get existing properties of data and return their value', () => {
      // Key should exist
      expect(gameState._data[existentKey]).not.toBeUndefined();

      spyOn(console, 'error');
      const data = gameState._data[existentKey];
      const result = gameState.get(existentKey);

      expect(console.error).not.toHaveBeenCalled();
      expect(result).toBe(data);
    });

    describe('set listeners', () => {
      beforeEach(() => {
        for (const property in gameState._data) {
          if (Object.prototype.hasOwnProperty.call(gameState._data, property)) {
            delete gameState._listeners[property];
          }
        }
      });

      describe('addSetListener', () => {
        it('should return undefined', () => {
          const result = gameState.addSetListener('currentLevel', () => {});
          expect(result).toBeUndefined();
        });

        it('should not affect standard getter/setter behavior', () => {
          gameState.set('currentLevel', 1);
          const valueBeforeListener = gameState.get('currentLevel');
          gameState.addSetListener('currentLevel', () => {});
          const valueAfterListener = gameState.get('currentLevel');
          const newLevel = 2;
          gameState.set('currentLevel', newLevel);
          const levelAfterSet = gameState.get('currentLevel');
          expect(valueBeforeListener).toBe(valueAfterListener);
          expect(newLevel).toBe(levelAfterSet);
        });

        it('should run all listener functions when `set` is ran for given property', () => {
          gameState.set('currentLevel', 1);
          const listeners = {
            onCurrentLevelChange() {},
            anotherOnCurrentLevelChange() {},
          };
          spyOn(listeners, 'onCurrentLevelChange');
          gameState.addSetListener('currentLevel', listeners.onCurrentLevelChange);
          gameState.set('currentLevel', 2);
          expect(listeners.onCurrentLevelChange).toHaveBeenCalledTimes(1);
          spyOn(listeners, 'anotherOnCurrentLevelChange');
          gameState.addSetListener('currentLevel', listeners.anotherOnCurrentLevelChange);
          gameState.set('currentLevel', 3);
          expect(listeners.onCurrentLevelChange).toHaveBeenCalledTimes(2);
          expect(listeners.anotherOnCurrentLevelChange).toHaveBeenCalledTimes(1);
        });

        it('should pass the new value to the callback', () => {
          gameState.set('currentLevel', 1);
          const listeners = {
            onCurrentLevelChange() {},
          };
          spyOn(listeners, 'onCurrentLevelChange');
          gameState.addSetListener('currentLevel', listeners.onCurrentLevelChange);
          const newCurrentLevel = 2;
          gameState.set('currentLevel', newCurrentLevel);
          expect(listeners.onCurrentLevelChange).toHaveBeenCalledOnceWith(newCurrentLevel);
        });

        it('should not add a listener if the property does not exist', () => {
          const property = 'nonExistentKey';
          gameState.addSetListener(property, () => {});
          expect(gameState._listeners[property]).toBeUndefined();
        });
      });

      describe('removeSetListener', () => {
        it('should return undefined', () => {
          function func () {}
          gameState.addSetListener('currentLevel', func);
          const result = gameState.removeSetListener('currentLevel', func);
          expect(result).toBeUndefined();
        });

        it('should remove any existing listener for a given property', () => {
          function func () {}
          gameState.addSetListener('currentLevel', func);
          gameState.removeSetListener('currentLevel', func);
          for (const property in gameState._listeners) {
            if (Object.prototype.hasOwnProperty.call(gameState._listeners, property)) {
              if (Array.isArray(gameState._listeners[property])) {
                expect(gameState._listeners[property]).toHaveSize(0);
              }
            }
          }
        });
      });
    });
  });

  describe('isInSession', () => {
    it('should return boolean', () => {
      const isInSession = gameState.isInSession();
      expect(isInSession).toBeInstanceOf(Boolean);
    });

    it('should reflect whether the gameState is inSession or not', () => {
      gameState.set('inSession', true);
      const isInSession = gameState.isInSession();
      gameState.set('inSession', false);
      const isNotInSession = gameState.isInSession();
      expect(isInSession).toBe(true);
      expect(isNotInSession).toBe(false);
    });
  });

  describe('allJacksGrabbed', () => {
    beforeEach(() => {
      gameState._data.jacks = makeJacks(4, mockPixi);
      expect(gameState._data.jacks.length).not.toBe(0);
    });

    it('should return a boolean', () => {
      expect(gameState.allJacksGrabbed()).toBeInstanceOf(Boolean);
    });

    it('should return false if not all jacks are grabbed', () => {
      gameState._data.jacks.forEach(jack => {
        jack.grabbed = false;
      });
      const allJacksGrabbed = gameState.allJacksGrabbed();
      expect(allJacksGrabbed).toBeFalse();
    });

    it('should return true if all jacks are grabbed', () => {
      gameState._data.jacks.forEach(jack => {
        jack.grabbed = true;
      });
      const allJacksGrabbed = gameState.allJacksGrabbed();
      expect(allJacksGrabbed).toBeTrue();
    });
  });

  describe('allJacksPlaced', () => {
    beforeEach(() => {
      gameState._data.jacks = makeJacks(4, mockPixi);
      expect(gameState._data.jacks.length).not.toBe(0);
    });

    it('should return a boolean', () => {
      expect(gameState.allJacksPlaced()).toBeInstanceOf(Boolean);
    });

    it('should return false if not all jacks are placed', () => {
      gameState._data.jacks.forEach(jack => {
        jack.isPlaced = false;
      });
      const allJacksPlaced = gameState.allJacksPlaced();
      expect(allJacksPlaced).toBeFalse();
    });

    it('should return true if all jacks are placed', () => {
      gameState._data.jacks.forEach(jack => {
        jack.isPlaced = true;
      });
      const allJacksPlaced = gameState.allJacksPlaced();
      expect(allJacksPlaced).toBeTrue();
    });
  });
});
