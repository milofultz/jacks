export default {
  Graphics: function () {
    this.beginFill = function(color) {};
    this.drawEllipse = function() {};
    this.drawRect = function() {};
    this.endFill = function() {};
  },
  Container: function() {
    this.children = [];
    this.addChild = function(child) {
      this.children.push(child);
    };
  },
  Sprite: function(image) {
    // do nothing as of yet
  },
  Texture: {
    from: function(imageURL) {
      // do nothing as of yet
    },
  },
};
