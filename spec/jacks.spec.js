import mockPixi from './mocks/mockPixi.js';
import constants from '../modules/constants.mjs';
import { correctNumberOfJacksGrabbed, getRandomPositions, makeJacks, moveGrabbedJacksOutOfPlay } from '../modules/jacks.mjs';

describe('makeJacks', () => {
  it('should return an array', () => {
    expect(makeJacks(0, mockPixi)).toBeInstanceOf(Array);
  });

  it('should return an array with length of argument', () => {
    const result_4 = makeJacks(4, mockPixi);
    const result_1 = makeJacks(1, mockPixi);
    expect(result_4.length).toBe(4);
    expect(result_1.length).toBe(1);
  });

  it('should return an array of PIXI.Sprites', () => {
    const results = makeJacks(4, mockPixi);
    results.forEach(jack => {
      expect(jack).toBeInstanceOf(mockPixi.Sprite);
    });
  });

  it('should each have unique IDs', () => {
    const idSet = new Set();
    const results = makeJacks(4, mockPixi);
    results.forEach(jack => idSet.add(jack.id));
    expect(idSet.size).toBe(results.length);
  });

  it('should have `isPlaced` and `grabbed` properties set to false', () => {
    const results = makeJacks(4, mockPixi);
    results.forEach(jack => {
      expect(jack.isPlaced).toBeFalse();
      expect(jack.grabbed).toBeFalse();
    });
  });
});

describe('getRandomPositions', () => {
  it('should return an array', () => {
    expect(getRandomPositions(1)).toBeInstanceOf(Array);
  });

  it('should return an array of objects', () => {
    const numberOfJacks = 4;
    const coordinates = getRandomPositions(numberOfJacks);
    expect(coordinates.length).toBe(numberOfJacks);
    coordinates.forEach(coord => {
      expect(coord).toBeInstanceOf(Object);
    });
  });

  it('should contain x and y properties with numeric values in each object', () => {
    const coordinates = getRandomPositions(4);
    coordinates.forEach(coord => {
      expect(coord.x).toBeInstanceOf(Number);
      expect(coord.y).toBeInstanceOf(Number);
    });
  });

  it('should have no overlapping coordinates in the full 4x4 grid', () => {
    const numberOfJacks = 16;
    const coordSet = new Set();

    const coordinates = getRandomPositions(numberOfJacks);
    coordinates.forEach(coord => {
      const hash = Math.pow(2, coord.x) * Math.pow(3, coord.y);
      expect(coordSet.has(hash)).toBeFalse();
      coordSet.add(hash);
    });

    expect(coordSet.size).toBe(numberOfJacks);
  });

  it('should return an empty array if numberOfJacks is greater than MAX_JACKS in constants', () => {
    const numberOfJacks = constants.MAX_JACKS + 1;
    const coordinates = getRandomPositions(numberOfJacks);
    expect(coordinates.length).toBe(0);
  });
});


describe('moveGrabbedJacksOutOfPlay', () => {
  let jacks;

  beforeEach(() => {
    const numberOfJacks = 4;
    jacks = makeJacks(numberOfJacks, mockPixi);
    expect(jacks.length).not.toBe(0);
    expect(jacks.filter(jack => jack.grabbed)).toHaveSize(0);
    expect(jacks.filter(jack => jack.inPlay)).toHaveSize(numberOfJacks);
  });

  it('should return undefined', () => {
    expect(moveGrabbedJacksOutOfPlay(jacks)).toBeUndefined();
  });

  it('should no nothing if nothing is passed in', () => {
    function func () {
      moveGrabbedJacksOutOfPlay();
    }
    expect(func).not.toThrow(TypeError);
  });

  it('should set `inPlay` to false for any jacks that are `grabbed`', () => {
    const jack = jacks[0];
    jack.grabbed = true;
    expect(jack.inPlay).toBeTrue();
    moveGrabbedJacksOutOfPlay(jacks);
    expect(jack.inPlay).toBeFalse();
  });
});

describe('correctNumberOfJacksGrabbed', () => {
  let currentLevel;
  let jacks;

  beforeEach(() => {
    currentLevel = 1;
    const numberOfJacks = 4;
    jacks = makeJacks(numberOfJacks, mockPixi);
    expect(jacks.length).not.toBe(0);
    expect(jacks.filter(jack => jack.grabbed)).toHaveSize(0);
    expect(jacks.filter(jack => jack.inPlay)).toHaveSize(numberOfJacks);
  });

  it('should return a boolean', () => {
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeInstanceOf(Boolean);
  });

  it('should return true if the number of jacks grabbed matches with the currentLevel number', () => {
    jacks[0].grabbed = true;
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeTrue();
    currentLevel = 2;
    jacks[1].grabbed = true;
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeTrue();
  });

  it('should return true if all jacks in play are grabbed and is less than currentLevel', () => {
    currentLevel = 5;
    jacks.forEach(jack => (jack.grabbed = true));
    // All four jacks were grabbed, is true even though currentLevel is 5
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeTrue();
  });

  it('should return false if less or more jacks are grabbed than currentLevel', () => {
    currentLevel = 2;
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeFalse();
    jacks[0].grabbed = true;
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeFalse();
    jacks[1].grabbed = true;
    jacks[2].grabbed = true;
    expect(correctNumberOfJacksGrabbed(currentLevel, jacks)).toBeFalse();
  });
});
