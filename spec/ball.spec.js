import { makeBall, makeShadow } from "../modules/ball.mjs";
import mockPixi from "./mocks/mockPixi.js";

describe('makeBall', () => {
  let ball;

  beforeEach(() => {
    ball = makeBall(mockPixi);
  });

  it('should return an instance of a PIXI Sprite', () => {
    expect(ball).toBeInstanceOf(mockPixi.Sprite);
  });

  it('should have a defined `zIndex` property greater than 1', () => {
    expect(ball.zIndex).not.toBeUndefined();
    expect(ball.zIndex).toBeGreaterThan(1);
  });

  it('should be interactive', () => {
    expect(ball.interactive).not.toBeUndefined();
    expect(ball.interactive).toBe(true);
  });
});

describe('makeShadow', () => {
  it('should return an instance of a PIXI Graphics', () => {
    const shadow = makeShadow(mockPixi);
    expect(shadow).toBeInstanceOf(mockPixi.Graphics);
  });
});

