import { makeBackground } from "../modules/background.mjs";
import mockPixi from "./mocks/mockPixi.js";

describe('makeBackground', () => {
  let background;

  beforeEach(() => {
    background = makeBackground(mockPixi);
  });

  it('should return an instance of a PIXI Container', () => {
    expect(background).toBeInstanceOf(mockPixi.Container);
  });

  it('should include two PIXI Sprites fpr the background and the picture', () => {
    const sprites = background.children.filter(child => child instanceof mockPixi.Sprite);
    expect(sprites).toHaveSize(2);
  });

  it('should include a PIXI Graphic used to cover over the original picture', () => {
    const graphics = background.children.filter(child => child instanceof mockPixi.Graphics);
    expect(graphics).toHaveSize(1);
  });
});

