import app from "./modules/pixiLoader.mjs";
import constants from './modules/constants.mjs';
import makeGameState from "./modules/gameState.mjs";
import { getBallAnimationValues } from "./modules/animationValues.mjs";
import { makeBackground } from "./modules/background.mjs";
import { correctNumberOfJacksGrabbed, makeJacks, moveGrabbedJacksOutOfPlay } from "./modules/jacks.mjs";
import { makeBall, makeShadow } from "./modules/ball.mjs";


// Prepare graphical elements and create variables
app.renderer.backgroundColor = 0xffffff;

const background = makeBackground(PIXI);
app.stage.addChild(background);

const gameState = makeGameState();

const shadow = makeShadow(PIXI);
app.stage.addChild(shadow);

const ball = makeBall(PIXI);
app.stage.addChild(ball);

let delta = 0;

// Get HTML elements
const difficultyElement = document.getElementById('difficulty');
const startingLevelElement = document.getElementById('starting-level');
const currentLevelElement = document.getElementById('current-level');
gameState.addSetListener('currentLevel', (value) => {
  currentLevelElement.textContent = value;
});

// Prepare helper and event functions

/**
 * Sets up the game state and creates the jacks for a new "session".
 *
 * @returns {void}
 */
function setUpNewSession () {
  gameState.set('inSession', false);
  gameState.set('ballIsGrabbed', false);

  gameState.get('jacks').forEach(jack => app.stage.removeChild(jack));

  const numberOfJacks = Math.max(constants.JACKS_DEFAULT_AMOUNT, gameState.get('currentLevel'));
  const jacks = makeJacks(numberOfJacks, PIXI);

  jacks.forEach(jack => {
    jack.on('mousedown', () => {
      if (gameState.isInSession()) {
        jack.grabbed = true;
        jack.visible = false;
      }
    });
    app.stage.addChild(jack);
  });

  gameState.set('jacks', jacks);
}

/**
 * Sets all gamestate values when starting a new game.
 *
 * @returns {void}
 */
function initializeNewGame () {
  const gameDifficulty = gameState.get('difficulty');
  const secondsInAir = constants.SECONDS_IN_AIR_BY_DIFFICULTY[gameDifficulty];

  const ballAnimationValues = getBallAnimationValues(secondsInAir);

  gameState.set('ballAnimationValues', ballAnimationValues);
  gameState.set('currentLevel', gameState.get('startingLevel'));
  setUpNewSession();
}

// Test code to set the difficulty and startingLevel
gameState.set('difficulty', difficultyElement.value);
difficultyElement.addEventListener('change', (e) => {
  gameState.set('difficulty', e.target.value);
  if (!gameState.isInSession()) {
    initializeNewGame();
  }
});

gameState.set('startingLevel', Number(startingLevelElement.value));
startingLevelElement.addEventListener('change', (e) => {
  const startingLevelValue = Number(e.target.value);
  gameState.set('startingLevel', startingLevelValue);
  if (!gameState.isInSession()) {
    gameState.set('currentLevel', startingLevelValue);
    initializeNewGame();
  }
});

/**
 * Handles what happens after a ball is grabbed after being in the air. Either
 * a session continues, a new session is created, or the game is over.
 *
 * @returns {void}
 */
function handleBallEnd () {
  ball.x = constants.BALL_START.x;
  ball.y = constants.BALL_START.y;
  shadow.x = constants.SHADOW_START.x;
  shadow.y = constants.SHADOW_START.y;
  delta = 0;

  const currentLevel = gameState.get('currentLevel');
  const jacks = gameState.get('jacks');
  if (gameState.get('ballIsGrabbed') && correctNumberOfJacksGrabbed(currentLevel, jacks)) {
    if (gameState.allJacksGrabbed()) {
      gameState.set('currentLevel', currentLevel + 1);
      setUpNewSession();
    } else {
      moveGrabbedJacksOutOfPlay(jacks);
    }
    gameState.set('inSession', false);
  } else {
    alert(constants.MESSAGES.LOSE);
    initializeNewGame();
  }
}

ball.on('mousedown', () => {
  if (gameState.isInSession()) {
    gameState.set('ballIsGrabbed', true);
    handleBallEnd();
  } else {
    // Start a new game
    ball.x = constants.BALL_START.x;
    ball.y = constants.BALL_START.y;
    shadow.x = constants.SHADOW_START.x;
    shadow.y = constants.SHADOW_START.y;
    gameState.set('inSession', true);
  }
});

// Create animation loop
setInterval(() => {
  // Jacks
  if (!gameState.allJacksPlaced()) {
    const jacks = gameState.get('jacks');
    jacks.forEach(jack => {
      if (jack.isPlaced) {
        return;
      }

      // Get all the jacks to their starting positions
      const speed = constants.JACKS_SPEED_ON_START;
      jack.x += jack.delta.x / speed;
      jack.y += jack.delta.y / speed;
      let offset;
      const [
        firstRow, // front row
        secondRow,
        thirdRow,
        fourthRow
      ] = constants.JACKS_GRID_SPACING.y;
      const [
        firstRowOffset,
        secondRowOffset,
        thirdRowOffset,
        fourthRowOffset
      ] = constants.JACKS_ROW_SIZE_OFFSET;
      if (jack.y >= firstRow) {
        offset = firstRowOffset;
      } else if (jack.y >= secondRow) {
        offset = secondRowOffset;
      } else if (jack.y >= thirdRow) {
        offset = thirdRowOffset;
      } else if (jack.y >= fourthRow) {
        offset = fourthRowOffset;
      }
      jack.width = constants.JACKS_SIZE.x * offset;
      jack.height = constants.JACKS_SIZE.y * offset;
      if ((jack.end.x - jack.x) < jack.delta.x / speed) {
        jack.x = jack.end.x;
        jack.y = jack.end.y;
        jack.isPlaced = true;
      }
    });
  }

  // Ball and shadow
  const { deltaStep, maxHeight, xStep } = gameState.get('ballAnimationValues');

  if (Math.abs(ball.y - constants.BALL_START.y) < deltaStep && delta !== 0) {
    handleBallEnd();
  } else if (gameState.get('inSession')) {
    delta += deltaStep;
    ball.y -= maxHeight * Math.cos(delta);
    ball.x -= xStep;
    shadow.x -= xStep;
  }
}, constants.FRAME);

initializeNewGame();

