## Tech Stack

Probably living inside the NPM/Yarn framework just because.

* Graphics: PIXI.js since I know it well enough.
* UI: Not React just because I don't like that whole thing. Maybe Vue, since that has interested me for a while.
* Server: Express
* DB: Just need it for basic basic stuff like saving high scores etc. Maybe implement a login situation for kicks. SQLite since I've wanted to use it for a while.
* Hosting: Heroku? Want to not use AWS because I hate Amazon's horrible interface and surprise charges all the time. May not be better but at least it's different.
* Testing: Been using Jest. Want to learn something else to get away from FB. What is there?
    * Cypress - Recommended by Vue
    * Vitest - [Also recommended](https://vuejs.org/guide/scaling-up/testing.html#unit-testing)
    * Mocha/Chai
    * Jasmine

May just end up going with all the Vue tooling that comes from a [vue create app thing](https://cli.vuejs.org/guide/creating-a-project.html#vue-create). In the future would probably want to start from the ground up as well, but for now, that will just be an accepted "this is okay" thing.

---

Things have already changed a bit. Decided to ditch Vue and any other FE framework for the sake of ease. Implementing PIXI with anything other than vanilla JS is a pain, and I don't want to fuck with that. I'll learn Vue fully when I find a good proj to do. I love it's layout, but not good for this project.

* Graphics: PIXI.js
* UI: Vanilla JS with Modules (`mjs`)
* Server: Express
* DB: SQLite maybe?
* Hosting: just not AWS?
* Testing: Jasmine

